
const uri = '/api/'
const userModel = require('../models/user')

module.exports = function(app) {
  let home = (req, res) => {
    res.send('a homepage')
  }

  app.get('', home)

  /*
  - As a user, I must be able to sign up with username, email and password
  - As a user, I must be able to login using either username or email and password
  - As a user, I must be able to update and save my account information
  - As a user, I must be able to logout
  - As a user, I must be able to get information of a mobile number using this public API -
  https://goo.gl/4JmdP1
  */

  let register = (req, res) => {
    userModel.create(req.body)
    .then((result) => {
      console.log('res: ', res)
      res.send({
        id: result._id
      })
    })
    .catch((err) => {
      console.log('err: ', err)
      res.status(400).send(err)
    })
  }

  app.post(uri + 'users', register)

  let auth = (req, res) => {
    userModel.auth(req.body)
    .then((result) => {
      console.log('res: ', result)
      res.send(result)
    })
    .catch((err) => {
      console.log('err: ', err)
      res.status(400).send(err)
    })
  }

  app.post(uri + 'auth', auth)

  let update = (req, res) => {
    userModel.update(req.params.id, req.body)
    .then((result) => {
      console.log('res: ', result)
      res.send(result)
    })
    .catch((err) => {
      console.log('err: ', err)
      res.status(400).send(err)
    })
  }

  app.put(uri + 'users/:id', update)

  let logout = (req, res) => {
    userModel.logout(req.headers.authorization)
    .then((result) => {
      console.log('res: ', result)
      res.send(result)
    })
    .catch((err) => {
      console.log('err: ', err)
      res.status(400).send(err)
    })
  }

  app.get(uri + 'logout', logout)

  let getMobile = (req, res) => {
    // https://numverify.com/documentation
    userModel.getMobile(req.body)
    .then((result) => {
      console.log('result: ', result)
      if (result.valid){
        res.send(result)
      }
      else {
        res.status(400).send({
          errors: {
            message: result.error.info
          }
        })
      }
    })
    .catch((err) => {
      console.log('err: ', err)
      res.status(400).send(err)
    })
  }

  app.post(uri + 'get-mobile', getMobile)

};

var fs = require('fs');

module.exports = function(app, passport) {
  fs.readdirSync('./routes').forEach(function(file) {
    if (file != 'index.js') {
      require('./' + file)(app);
    }
  });
};

let Promise = require('bluebird')
let jwt = require('jsonwebtoken-promisified')
let bcrypt = require('bcrypt-nodejs-as-promised')
let bcrypt2 = require('bcrypt-nodejs')
let _ = require('lodash')
let config = require('../config')
let mongoDb = config.mongoDb
let mongoose = require('mongoose')
let axios = require('axios')

console.log('mongoDb: ', mongoDb)

mongoose.Promise = require('bluebird')

let Schema = mongoose.Schema
let connection = mongoose.createConnection(mongoDb)

const userSchema = new Schema({
  username: {
    type: String,
    minlength: 5,
    unique: true,
    required: true,
  },

  email: {
    type: String,
    unique: true,
    required: true,
  },

  password: {
    type: String,
    minlength: 5,
    required: true,
    select: false
  }
})

let User = connection.model('User', userSchema);

// Create a user
module.exports.create = (data) => {
  console.log('create user: ', data)
  return new Promise((resolve, reject) => {
    if (_.isEmpty(data)) {
      err = {
        errors: {
          message: 'User data is required.'
        }
      };
      reject(err)
    } else {
        /*data.password = bcrypt.hashSync(data.password)
        let user = new User(data)*/
        return new Promise((resolve, reject) => {
          bcrypt2.genSalt(10, (err, salt) => {
            if (err) {
              return reject(err);
            }
            bcrypt2.hash(data.password, salt, null, (err, hash) => {
              if (err) {
                return reject(err)
              }
              return resolve(hash)
            })
          })
        })
        .then((hash) =>{
          data.password = hash
          let user = new User(data)
          return user.save()
        })
        .then((result) => {
          console.log('New user created: ', result.id);
          resolve(result)
        })
        .catch((error) => {
          console.log('error: ', error)
          return reject(error)
        })
    }
  })
}

// auth user
module.exports.auth = (data) => {
  console.log('auth user: ', data)
  return new Promise((resolve, reject) => {
    let userData = ''
    User.findOne({
      $or : [
        {
          username: data.username
        },
        {
          email: data.email
        }
      ]

    })
    .select('username email password')
    .then((result) => {
      console.log('result: ', result)
      userData = result
      if (!result) {
        reject({
          errors: {
            message: 'User does not exists.'
          }
        })
      }
      return bcrypt.compare(data.password, result.password)
    })
    .then((res, error) => {
      if(res) {
        return jwt.signAsync({
          id: userData._id
        }, config.secret)
      }
      else {
        reject({
          errors: {
            message: error
          }
        })
      }
    })
    .then((token) => {
      resolve({
        token: token,
        id: userData._id,
        username: userData.username,
        email: userData.email
      })
    })
    .catch((error) => {
      reject(error)
    })
  })
}

module.exports.update = (id, data) => {
  return new Promise((resolve, reject) => {
    if(data.password) {
      data.password = bcrypt.hashSync(data.password)
    }
    User.findOneAndUpdate({
      _id: id
    }, data)
    .then((res) => {
      resolve(res)
    })
    .catch((error) => {
      reject(error)
    })
  })
}

module.exports.logout = (token) => {
  return new Promise((resolve, reject) => {
    // do destroy
    resolve({ message: 'destroy' })
  })
}

module.exports.getMobile = (data) => {
  return axios.get(config.endpoint, {
    params: {
      access_key: config.apiKey,
      number: data.number
    }
  })
  .then((res) => {
    return res.data
  })
}

var config = require('./config');
var express = require('express');
var session = require('express-session');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var cors = require('cors');
var helmet = require('helmet');
var Promise = require('bluebird');
var jwt = require('jsonwebtoken-promisified');
var app = express();

app.use(cors()); // enable cors in all routes

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
// app.use(express.static(path.join(__dirname, 'public')));

//security (mostly header stuff)
app.use(helmet());

// Force SSL
/* istanbul ignore if */
if (process.env.forceSSL == 'true') {
  app.use(function (req, res, next) {
    if (req.headers['x-forwarded-proto'] != 'https')
      return res.redirect(['https://', req.get('Host'), req.url].join(''));
    return next();
  });
}

// middleware
app.use((req, res, next) => {
  var public = ['/', '/api/users', '/api/auth'];

  if(public.indexOf(req.path) > -1){
    next()
  }
  else {
    var token = req.headers.authorization
    console.log('validate jwt')
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        res.status(401).send({
          errors: {
            message: 'Invalid Token'
          }
        })
      }
    })
  }
})

require('./routes')(app);

/**
 * Error Handlers
 */
/* istanbul ignore next */
process.on('uncaughtException', function (err) {
  // loggers.error((new Date).toUTCString() + ' uncaughtException:', err.message);
  // loggers.error(err.stack);
  process.exit(1);
});


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// development error handler
// will print stacktrace
/* istanbul ignore if */
if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
